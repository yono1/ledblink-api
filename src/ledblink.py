#!/usr/bin/env python3
import RPi.GPIO as GPIO
import os
from datetime import datetime
import logging
import json
from flask import Flask,request,make_response

# instantiate the API
app = Flask(__name__)

# 環境変数を取得
SERVICE_PORT = "5000" if (os.environ.get("SERVICE_PORT") is None) else os.environ.get("SERVICE_PORT")

# Loggerを初期化
logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
log = logging.getLogger(__name__)

# initial LED state
LED_STATE = {
    "Red": "OFF",
    "Blue": "OFF",
    "White": "OFF"
    }

# GPIO_PIN Array
GPIO_PIN = {
    "Red": 17,
    "Blue": 27,
    "White": 22
}

# Turn On LED
def turn_on( LedPin: int, Color: str ):
    GPIO.output(LedPin, GPIO.LOW)
    LED_STATE[ Color ] = "ON"

# Tunr Off LED
def turn_off( LedPin: int, Color: str ):
    GPIO.output(LedPin, GPIO.HIGH)
    LED_STATE[ Color ] = "OFF"
    # Release resource
    GPIO.cleanup()

# Get Led Status
@app.route("/", methods=["GET"])
def get_led_state():
    return LED_STATE

# Change Led Status
@app.route("/", methods=["POST"])
def change_led_state():
    # POSTデータを取得
    data = json.loads( request.data )
    Color = str(data["Color"])
    State = str(data["State"])

    LedPin = GPIO_PIN[ Color ]

    GPIO.setmode(GPIO.BCM)
    GPIO.setup(LedPin, GPIO.OUT, initial=GPIO.HIGH)
    log.info(f"POST data, LED_COLOR:{Color}, LED_PIN: {LedPin}, STATE:{State}")

    if State == "ON":
        turn_on(LedPin, Color)
    else:
        turn_off(LedPin, Color)

    return LED_STATE

# If run this script directly, do:
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=int(SERVICE_PORT))

