# LED Blink REST API

ラズパイ4でのLED点灯をREST APIで制御

## Getting started
```bash
python ledblink.yp
```

### Get LED Status
`http://<ラズパイのIPアドレス>:5000/` へHTTP GETすると、以下の通りLEDの現在の状態を取得します。

```
{
    "Red": "OFF",
    "Blue": "OFF",
    "White": "OFF",
}
```

### Change LED Status

`http://<ラズパイのIPアドレス>:5000/` へ以下のデータをPOSTすると、POSTデータの通りにLEDが点灯します。

```
{
    "Color": "Red",
    "State": "ON"
}
```